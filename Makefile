install: hooks
	cd ./docker && ./install.sh

update:
	cd ./docker && ./update.sh

start: hooks
	cd ./docker && ./start.sh

stop:
	cd ./docker && ./stop.sh

restart: stop start

hooks:
	echo "#!/bin/bash" > .git/hooks/pre-commit
	echo "make check" >> .git/hooks/pre-commit
	chmod +x .git/hooks/pre-commit

mysql:
	cd ./docker && source .env && docker-compose exec mysql mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE}

xdebug-enable:
	cd ./docker && docker-compose exec -T -u 0 php docker-php-ext-enable xdebug
	cd ./docker && docker-compose exec -T -u 0 php bash -c "kill -USR2 1"

xdebug-disable:
	cd ./docker && docker-compose exec -T -u 0 php sed -i '/zend_extension/d' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
	cd ./docker && docker-compose exec -T -u 0 php bash -c "kill -USR2 1"

shell:
	cd ./docker && docker-compose exec php bash

clear-cache:
	bin/console cache:clear

migrations:
	bin/console doctrine:database:create --if-not-exists
	bin/console doctrine:migration:migrate --allow-no-migration --no-interaction

check:
	cd ./docker/ && docker-compose exec -T php make phpcs
	cd ./docker/ && docker-compose exec -T php make stan
	cd ./docker/ && docker-compose exec -T php make doctrine
	cd ./docker/ && docker-compose exec -T php make security

phpcs:
	vendor/bin/phpcs

fixer:
	./vendor/bin/phpcbf

stan:
	bin/console cache:warmup --env=dev
	vendor/bin/phpstan analyse src --level max --memory-limit=4000M

doctrine:
	bin/console d:s:v
	bin/console d:s:u --dump-sql

security:
	security-checker security:check
